﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ArabicLexicon.Model;
using ArabicLexicon.Model.Entities;
using ArabicLexicon.Service;
using ArabicLexicon.Service.Test;

namespace ArabicLexicon.Web.Controllers
{
    public class StackOverflowController : Controller
    {
        public StackOverflowController()
        {
        }
        public JsonResult GetStackOverflowItems()
        {
            string url = @"https://api.stackexchange.com/2.2/questions?pagesize=50&order=desc&sort=creation&site=stackoverflow";
            string data = Get(url);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.ContentType = @"application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
