﻿import { NgModule, ErrorHandler } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule, MdNativeDateModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { routing } from './app.routing';
import { StackOverflowComponent } from "./components/stack-overflow.component";
import { StackOverflowService } from "./services/stack-overflow.service";
import { QuestionDetailsComponent } from "./components/question-details.component";



@NgModule({
    imports: [BrowserModule, ReactiveFormsModule, HttpModule, routing, FormsModule,
        BrowserAnimationsModule,
        MaterialModule,
        MdNativeDateModule],
    declarations: [AppComponent, StackOverflowComponent, QuestionDetailsComponent],
    providers: [
       
        { provide: APP_BASE_HREF, useValue: '/' }, StackOverflowService],
    
    bootstrap: [AppComponent],
    //entryComponents: [StackOverflowComponent]

})
export class AppModule { }
