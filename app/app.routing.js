"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var stack_overflow_component_1 = require("./components/stack-overflow.component");
var question_details_component_1 = require("./components/question-details.component");
var appRoutes = [
    //  { path: '', redirectTo: 'stackOverflow', pathMatch: 'full' },
    { path: '', component: stack_overflow_component_1.StackOverflowComponent, pathMatch: 'full' },
    { path: 'details/:id', component: question_details_component_1.QuestionDetailsComponent }
    //{ path: 'user', component: UserComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map