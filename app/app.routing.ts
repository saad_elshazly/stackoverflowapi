﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StackOverflowComponent } from "./components/stack-overflow.component";
import { QuestionDetailsComponent } from "./components/question-details.component";


const appRoutes: Routes = [
  //  { path: '', redirectTo: 'stackOverflow', pathMatch: 'full' },
    { path: '', component: StackOverflowComponent, pathMatch: 'full' },
    { path: 'details/:id', component: QuestionDetailsComponent }
     
    
    //{ path: 'user', component: UserComponent }
];

export const routing: ModuleWithProviders =
    RouterModule.forRoot(appRoutes);