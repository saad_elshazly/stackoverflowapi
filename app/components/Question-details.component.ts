﻿import { Component, OnInit } from '@angular/core';
import { StackOverflowService } from "../services/stack-overflow.service";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
    templateUrl: "/app/views/question-details.component.html"
})
export class QuestionDetailsComponent implements OnInit {

    Title: string = "Question Details";
    data: any;
  
    constructor(private route: ActivatedRoute, private service: StackOverflowService) {

    }
    ngOnInit(): void {
        this.route.params.subscribe(params => {
            console.log(params) //log the entire params object
            console.log(+params['id']) //log the value of id
            var id = +params['id'];
            this.getQuestionById(id);
        });
       
    }
    getQuestionById(id: number) {
        this.data = new Array();
        this.service.getDetails("https://api.stackexchange.com/2.2/questions/"+id+"?site=stackoverflow").subscribe(response => {
            // console.log(response);
            this.data = response.items[0];
           
        }, error => { });

    }
    public DateConvert(dt: string): string {
        var date = dt;
        var result = "No Data";
        if (dt != null) {
            var nowDate = new Date(parseInt(dt));
            var utc = nowDate.toLocaleDateString();
            var year = nowDate.getFullYear();
            var month = nowDate.getUTCMonth();
            var day = nowDate.getDay();
            result = utc;
        }
        return result;
    }

}