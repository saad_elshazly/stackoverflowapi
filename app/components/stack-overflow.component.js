"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var stack_overflow_service_1 = require("../services/stack-overflow.service");
var StackOverflowComponent = (function () {
    function StackOverflowComponent(stackOverflowService) {
        this.stackOverflowService = stackOverflowService;
        this.Title = "Stack Overflow Last 50 Questions";
    }
    StackOverflowComponent.prototype.ngOnInit = function () {
        this.getStackOverflowQuestions();
    };
    StackOverflowComponent.prototype.getStackOverflowQuestions = function () {
        var _this = this;
        this.btnSuccess = "loading...";
        this.stackOverflowService.get("StackOverflow/GetStackOverflowItems").subscribe(function (response) {
            var json = JSON.parse(response);
            _this.data = json.items;
            _this.btnSuccess = "Refresh";
        }, function (error) { });
    };
    StackOverflowComponent = __decorate([
        core_1.Component({
            templateUrl: "/app/views/stack-overflow.component.html"
        }),
        __metadata("design:paramtypes", [stack_overflow_service_1.StackOverflowService])
    ], StackOverflowComponent);
    return StackOverflowComponent;
}());
exports.StackOverflowComponent = StackOverflowComponent;
//# sourceMappingURL=stack-overflow.component.js.map