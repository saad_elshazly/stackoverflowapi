﻿import { Component,OnInit } from '@angular/core';
import { StackOverflowService } from "../services/stack-overflow.service";
@Component({
    templateUrl:"/app/views/stack-overflow.component.html"
})
export class StackOverflowComponent implements OnInit {
   
    Title: string = "Stack Overflow Last 50 Questions";
    data: any[];
    btnSuccess: string;
    constructor(private stackOverflowService: StackOverflowService) {
    }
    ngOnInit(): void {
        this.getStackOverflowQuestions();
    }
    getStackOverflowQuestions() {
        this.btnSuccess = "loading...";
        this.stackOverflowService.get("StackOverflow/GetStackOverflowItems").subscribe(response => {
            var json = JSON.parse(response)
            this.data = json.items;
            this.btnSuccess = "Refresh";
        }, error => { });

    }
    
}