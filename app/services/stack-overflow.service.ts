﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class StackOverflowService {
    constructor(private _http: Http) { }
    get(url: string): Observable<any> {
        //return this._http.get("https://api.stackexchange.com/2.2/questions?pagesize=50&order=desc&sort=creation&site=stackoverflow")
        return this._http.get(url)
            .map((response: Response) => <any>response.json());
    }
    getDetails(url: string): Observable<any> {
        return this._http.get(url)
            //return this._http.get(url)
            .map((response: Response) => <any>response.json());
    }
    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}