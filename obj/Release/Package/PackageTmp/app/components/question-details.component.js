"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var stack_overflow_service_1 = require("../services/stack-overflow.service");
var router_1 = require("@angular/router");
var QuestionDetailsComponent = (function () {
    function QuestionDetailsComponent(route, service) {
        this.route = route;
        this.service = service;
        this.Title = "Question Details";
    }
    QuestionDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            console.log(params); //log the entire params object
            console.log(+params['id']); //log the value of id
            var id = +params['id'];
            _this.getQuestionById(id);
        });
    };
    QuestionDetailsComponent.prototype.getQuestionById = function (id) {
        var _this = this;
        this.data = new Array();
        this.service.getDetails("https://api.stackexchange.com/2.2/questions/" + id + "?site=stackoverflow").subscribe(function (response) {
            // console.log(response);
            _this.data = response.items[0];
        }, function (error) { });
    };
    QuestionDetailsComponent.prototype.DateConvert = function (dt) {
        var date = dt;
        var result = "No Data";
        if (dt != null) {
            var nowDate = new Date(parseInt(dt));
            var utc = nowDate.toLocaleDateString();
            var year = nowDate.getFullYear();
            var month = nowDate.getUTCMonth();
            var day = nowDate.getDay();
            result = utc;
        }
        return result;
    };
    QuestionDetailsComponent = __decorate([
        core_1.Component({
            templateUrl: "/app/views/question-details.component.html"
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute, stack_overflow_service_1.StackOverflowService])
    ], QuestionDetailsComponent);
    return QuestionDetailsComponent;
}());
exports.QuestionDetailsComponent = QuestionDetailsComponent;
//# sourceMappingURL=question-details.component.js.map